---
title: Setting GUI on wsl2
date: 2022-04-15
categories:
 - others
tags:
 - docker
---

### Điều kiện
- Windows 10, 11
- Cài wsl2 bản mới nhất
- Cài Docker

### Setting 
```shell
$ echo `export DISPLAY="`grep nameserver /etc/resolv.conf | sed 's/nameserver //'`:0"` >> ~/.profile
# start Xlaunch application
$ xeyes
```
