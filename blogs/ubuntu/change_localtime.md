---
title: Change localtime on Ubuntu
date: 2021-12-03
categories:
 - others
tags:
 - ubuntu
---
Để tiện cho việc test và debug trên hệ thống, đôi khi chúng ta cần thay đổi local time.

## Ngắt chức năng cập nhật thời gian
Đầu tiên chúng ta sẽ phải ngắt chức năng tự động cập nhật thời gian của hệ thống:
```
$ sudo  timedatectl set-ntp no
```

## Thay đổi local time
```
$ timedatectl set-time 21:45:53
```
hoặc thay đổi ngày tháng
```
$ timedatectl set-time 2019-04-10
```
hoặc thay đổi cả ngày tháng và giờ
```
$ timedatectl set-time "2019-04-10 21:45:53"
```
kiểm tra lại xem kết quả đã thay đổi chưa
```
$ date
```

## Bật lại chức năng cập nhật thời gian
Sau khi test xong thì bật lại chức năng tự động cập nhật thời gian
```
$ sudo timedatectl set-ntp yes
```
