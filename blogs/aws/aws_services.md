---
title: Các dịch vụ của AWS
date: 2021-10-04
categories:
 - aws
tags:
 - aws
---
Trong bài viết này sẽ tổng hợp các dịch vụ nổi bật của AWS

## AWS có tất cả bao nhiêu dịch vụ?
Không có con số chính xác cho câu hỏi này, và tính đến thời điểm hiện tại 2021/10/04 thì AWS cung cấp hơn 200 services trải rộng trong các lĩnh vực về công nghệ và công nghiệp.

## Top 15 dịch vụ của AWS
### Service #1 Amazon S3
S3 là viết tắt của Simple Storage Service. S3 nổi tiếng là một dịch vụ lưu trữ dữ liệu trên cloud. Với những ưu điểm như
- Cho upload, download data với tốc độ cao, đặc biệt cho upload file có dụng lượng lớn tới 5TB
- Dễ mở rộng
- Rẻ
- Dễ dàng quản lý phân quyền cho data

### Service #2 Amazon EC2
EC2 = Elastic Compute Cloud. Đây là một dịch vụ cloud cung cấp khả năng tính toán và dễ dàng mở rộng. Người ta thường dùng EC2 để triển khai các ứng dụng một cách dễ dàng với chi phí thấp.

### Service #3 AWS Lambda
Dịch vụ Lambda của Amazon cho phép người dùng có thể chạy code mà không cần máy chủ. Lợi ích chính của nó là:
- Không cần mất công quản lý máy chủ
- Cho phép người dùng có thể dùng đến đâu trả tới đó, tránh trường lúc nào cũng phải chạy máy chủ. Nhờ vậy tối ưu hóa chi phí
- Hỗ trợ các ngôn ngũ nổi tiếng như Nodejs, Java, Python, Go và nhiều ngôn khác nữa
- Đáp ứng được mọi quy mô nhờ việc có thể tự thay quy mô ứng dụng

### Service #4 Amazon Glacier
Một dịch vụ lưu trữ gần giống như với S3. Sau đây là những điểm giống và khác nhau giữa Glacier và S3
- S3 đã rẻ nhưng Glacier thì còn rẻ hơn rất rất nhiều
- Bảo mật: cả 2 đều cao như nhau
- Tính khả dụng: cả 2 đều cao như nhau
- Cả 2 đều không giới hạn khả năng lưu trữ
- S3 phù hợp với những dữ liệu được truy cập thường xuyên, còn Glacier thì phù hợp với những dữ liệu ít thường xuyên được truy cập, vì vậy thường dùng nó cho những data backup, data lưu trữ với dữ liệu cực lớn
- S3 cho phép tối đa 5TB trên một file, còn Glacier cho phép lên tới 40TB một file
- S3 có thể được dùng để làm máy chủ cho trang web tĩnh, còn Glacier thì không cung cấp tính năng này
- S3 hỗ trợ việc quản lý các phiên bản của dữ liệu
- S3 hỗ trợ việc chạy phân tích và truy vấn dữ liệu
- Dữ liệu từ S3 có thể được đi chuyển sang các S3 khác hoặc thậm chí sang Glacier. Còn từ Glacier thì không di chuyển dữ liệu sang S3 được

### Service #5 Amazon SNS
Simple Notification Service, đây là dịch vụ cấp tin nhắn thông báo. Tin nhắn, thông báo sẽ hoạt động theo hình thức bên phát hành (publishers) và bên đăng ký(subscribers). Bên phát hành sẽ cung cấp thông báo đến
tất cả những bên đăng ký thông qua những kênh truyền tin riêng.

### Service #6 Amazon CloudFront
Giống như một CDN (Content Delivery Network), CloudFront là một địch vụ cung cấp mạng phân phôi nội dung trang web giúp tăng tốc tốc độ trang web. Nó dùng được cho cả web tĩnh và web động.  
Sử dụng CloudFront để truyền nội dung nhanh với độ trễ thấp và bảo mật cao.

### Service #7 Amazon EBS
EBS = Elastic Block Store  
Cũng là một dịch vụ lưu trữ data của Amazon. Nhưng nó có nhiều điểm rất khác với S3.
- EBS được trực tiếp gắn ghép với EC2 nên việc đọc file, ghi file sẽ nhanh hơn S3 rất nhiều.
- Có thể cài đặt database, software lên trên EBS. Còn S3 thì không (S3 = Simple Storage Service, mà database thì không simple)
- Có thể gắn kết EBS đã được cài đặt với tất cả các instance EC2 nào khác để tạo môi trường test chung tiện lợi
- Data lưu trữ trên S3 được tính theo object level data storage. Data được lưu trữ phân tán trên nhiều máy tính và cho phép người dùng có thể truy cập từ bất cứ đâu.
- Data lưu trữ trên EBS được tính theo block level data storage. Data được lưu trữ trên nhiều volume, được gọi là block, giống như việc lưu trên trên ổ ứng của một máy tính.
Cơ chế lưu trữ này không cho phép người dùng truy cập vào data từ bên ngoài thông qua internet như S3, mà chỉ có thể được truy cập từ instance EC2 được kết nối với nó.

### Service #8 Amazon Kinesis
Kinesis cung cấp một giải pháp để xử lý dữ liệu lớn một cách realtime. Kinesis cho phép các nhà phát triển có thể lấy bất cứ dữ liệu lớn nào từ bất cứ nguồn nào để xử lý được trên EC2.

### Service #9 Amazon VPC
Virtual Private Cloud giúp cho data, thông tin lưu trữ trên Amazon cloud được bảo mật. Nó như một VPN(Virtual Private Network), nhờ đó chỉ có những người được cấp quyền mới được quyền truy cập data.

### Service #10 Amazon SQS
Simple Queue Service, đây là dịch vụ quản lý messeage queue (xếp hàng tin nhắn). Nó cho phép chuyển dữ liệu hay tin nhắn từ ứng dụng này đến ứng dụng khác, kể cả khi ứng dụng đó đang không ở trạng thái hoạt động. Nó có thể 
gửi tin nhắn giữa các dịch vụ, ví dụ như S3, DynamoDB, EC2. Thời gian timeout mà tin nhắn có thể tồn tại lên tới tối đa 12 tiếng.

### Service #11 Amazon Elatics Beanstalk
Nhà phát triển có thể triển khai các dịch vụ, ứng dụng web được phát triển bằng .NET, Python, Java, PHP lên cloud mà không cần phải cài đặt gì thêm vào môi trường. 

### Service #12 DynamoDB
Một service để quản lý các cơ sở dữ liệu dạng NoSQL. Nó giúp nhà phát triển có thể tạo ra bảng dữ liệu, có thể lấy ra hoặc lưu trữ bất cứ loại dữ liệu nào. Dịch vụ này cũng giúp nhà phát triển điều khiển được lưu lượng data giữa các server, 
duy trì tính hiệu suất của các bảng dữ liệu.

### Service #13 Amazon RDS
Relation Database Service. Đây là dịch vụ cho phép tạo ra và quản lý các cơ sở dữ liệu SQL.

### Service #14 Amazon ElastiCache
Là một dịch vụ cung cấp in memory cache system trên cloud. Nó support cho Redis, MemCached. Sử dụng dịch vụ này sẽ tăng được hiệu suất của memory giúp cho việc đọc và ghi kết quả nhanh hơn.

### Service #15 Redshift
Dịch vụ quản lý data warehouse trên cloud. Sử dụng Redshift, khi thực hiện truy vấn lớn(mất nhiều thời gian) thì nó sẽ chia nhỏ truy vấn đó thành nhiều phần nhỏ, giao chúng cho nhiều node để xử lý song song.


### Nguồn tham khảo
- [What is aws](https://aws.amazon.com/what-is-aws/)
- [Tops AWS services](https://mindmajix.com/top-aws-services)
- [Difference Between AWS S3 and Glacier](https://tutorialsdojo.com/amazon-s3-vs-glacier/)
- [Difference Between AWS S3 and AWS EBS](https://www.geeksforgeeks.org/difference-between-aws-s3-and-aws-ebs/)